from math import copysign
from math import sqrt
from copy import deepcopy
#import toolutils
#Solver = toolutils.createModuleFromSection('SolverModule', kwargs['type'], 'SolverModule')


###########
# CLASSES #
###########

#Matrix class
class Matrix(object):
    def __init__(self, row, col, val=None):
        self.row = row
        self.col = col
        self.data = []
        for i in range(row):
            mat_col = []
            for j in range(col):
                mat_col.append(val)
            self.data.append(mat_col)            
            
    def __setitem__(self, pos, val):
        self.data[pos[0]][pos[1]] = val

    def __getitem__(self, pos):
        return self.data[pos[0]][pos[1]]

    def __str__(self):
        outStr = ""
        for i in range(self.row):
            outStr += 'Row %s = %s\n' % (i+1, self.data[i])
        return outStr

    def __mul__(self,other):
        if isinstance(other,int) or isinstance(other,float):
            result = Matrix(self.row, self.col, 0)
            for i in range(self.row):
                for j in range(self.col):
                    result[i,j] = self.data[i][j] * other
            return result
        #matrix "vector" multiplication, post multiply
        #check for the transpose bit on vectors
        elif isinstance(other,Vector):
            a = len(other)
            if a != self.col:
                return 'The number of columns do not equal the number of rows in the vector.'
            else:
                result = Vector(self.row, 0.0)
                for i in range(self.row):
                    sm = 0.0
                    for j in range(self.col):
                        sm = sm + self.data[i][j] * other[j]
                    result[i] = sm
                return result
        #matrix matrix multiplication 
        elif isinstance(other, Matrix):
            if self.col != other.row:
                return 'The number of columns of the first matrix must be equal to the number of rows of the second.'
            else:
                result = Matrix(self.row, other.col, 0)
                for i in range(self.row):
                    for j in range(other.col):
                        for k in range(other.row):
                             result[i,j] = result[i,j] + self.data[i][k] * other[k,j]               
                return result

    def __div__(self,other):
        if isinstance(other,int) or isinstance(other,float):
            result = Matrix(self.row, self.col, 0)
            for i in range(self.row):
                for j in range(self.col):
                    result[i,j] = self.data[i][j] / other
            return result        

    def __add__(self, other):
        if isinstance(other,int) or isinstance(other,float):
            result = Matrix(self.row, self.col, 0)
            for i in range(self.row):
                for j in range(self.col):
                    result[i,j] = self.data[i][j] + other
            return result
        elif self.col != other.col and self.row != other.row: 
            return 'Matrices are not equal.'
        else:
            for i in range(self.row):
                for j in range(self.col):
                    self.data[i][j] = self.data[i][j] + other[i,j]               
            return self

    def __sub__(self,other):
        if isinstance(other,Matrix):
            if self.col != other.col and self.cow != other.row:
                return 'Matrices do not have an identical size.'
            else:
                result = Matrix(self.row, self.col, 0)
                for i in range(self.row):
                    for j in range(self.col):
                        result[i,j] = self.data[i][j] - other[i,j]
                return result

    def __repr__(self):
        return 'Matrix(%d, %d)' % (self.row, self.col)

    def size(self):
        return (self.row, self.col)

    def setDiagonal(self, val):
        size = len(val)
        for i in range(size):
            self.data[i][i] = val[i]
#end matrix class


#and a VECTOR class
#to make matters confusing we'll stick to the representation of vectors
#as is done in most math and physics books, i.e. vectors are column
#vectors and transposed vectors are row vectors. This also means we post
#multiply vectors with matrices M * v. Houdini's way should be done like
#this v(t) * M(t) to get the same result.
class Vector(object):
    def __init__(self, size, val=None, trans=0):
        self.row = size
        self.transp = trans
        self.data = []
        for i in range(size):
            self.data.append(val)            
            
    def __setitem__(self, pos, val):
        self.data[pos] = val

    def __getitem__(self, pos):
        return self.data[pos]

    def __len__(self):
       return self.row 

    def __str__(self):
        outStr = "Vector= "
        for i in range(self.row):
            outStr += '%s ' % (self.data[i])
        if self.transp == 1:
            outStr += " (transposed)"
        return outStr

    def __add__(self,other):
        res = Vector(self.row,0.0)
        if isinstance(other,int) or isinstance(other,float):
            for i in range(self.row):
                res[i] = self[i] + other
        elif isinstance(other,Vector):
            for i in range(self.row):
                res[i] = self[i] + other[i]
        else:
            raise TypeError ("Unknown type for vector addition.")
        return res

    def __sub__(self,other):
        res = Vector(self.row,0.0)
        if isinstance(other,int) or isinstance(other,float):
            for i in range(self.row):
                res[i] = self[i] - other
        elif isinstance(other,Vector):
            for i in range(self.row):
                res[i] = self[i] - other[i]
        else:
            raise TypeError ("Unknown type for vector substraction.")
        return res

    def __mul__(self,other):
        if isinstance(other,int) or isinstance(other,float):
            res = Vector(self.row,0.0)
            for i in range(self.row):
                res[i] = self[i] * other
            return res
        #transposed vectors * vector give the dot product
        #vectors * transposed vector give the dyadic product
        #vector * vector gives the cross product
        elif isinstance(other,Vector):
            if self.row == other.row:
                if self.transp == 0 and other.transp == 1:
                    #tensor/dyadic or outer product
                    if other.transp == 1:
                        res = Matrix(self.row, self.row, 0.0)
                        for i in range(self.row):
                            for j in range(self.row):
                                res[i,j] = self[i] * other[j]
                        return res
                elif self.transp == 1 and other.transp ==0:
                    #dot or inner product
                    if other.transp == 0:
                        res = 0.0
                        for i in range(self.row):
                            res += self[i] * other[i]
                        return res
                else:
                    #cross product because both vectors are row or column vectors
                    if self.row == 3:
                        res = Vector(3,0.0)
                        res[0] = self[1] * other[2] - other[1] * self[2]
                        res[1] = self[2] * other[0] - other[2] * self[0]
                        res[2] = self[0] * other[1] - other[0] * self[1]
                        return res
                    else:
                        raise hou.Error("Cross prodcut of vectors with more than 3 dimensions not implemented.")
            else:
                raise hou.Error("Vectors not of equal length.")
        #vector matrix multiplication shouldn't be here but in matrix class
        #since we are using the math form of matrix vector multiplication which is post-mult
        elif isinstance(other,Matrix):
            if self.row != other.size()[1]:
                raise LookupError("Rows of vector not equal to column of matrix.")
            else:
                res = Vector(other.size()[1],0.0)
                for i in range(self.row):
                    sums = 0.0
                    for j in range(other.size()[1]):
                        sums += self[i] * other[i,j]
                    res[i] = sums
                return res

    def __neg__(self):
        res = Vector(self.row,0.0)       
        for i in range(self.row):
            res[i] = -self[i]
        return res

    @property
    def length(self):
        sum = 0.0
        for i in range(self.row):
            sum += self[i] * self[i]
        return sqrt(sum)

    @property
    def lengthSquared(self):
        sum = 0.0
        for i in range(self.row):
            sum += self[i] * self[i]
        return sum
  
    @property
    def max(self):
        max = 0.0
        for i in range(self.row):
            if max < self[i]:
                max = self[i]
        return max

    @property
    def min(self):
        min = 0.0
        for i in range(self.row):
            if min < self[i]:
                min = self[i]
        return min

    @property
    def maxabs(self):
        max = 0.0
        for i in range(self.row):
            if max < abs(self[i]):
                max = abs(self[i])
        return max

    @property
    def transpose(self):
        if self.transp == 0:
            self.transp = 1
        else:
            self.transp = 0

    def normalize(self):
        l = self.length
        for i in range(self.row):
            self[i] = self[i] / l

    def angle(self,other):
        if isinstance(other,Vector):
            res = 0.0
            if self.row == other.row:
                for i in range(self.row):
                    res += self[i] * other[i]
                l1 = self.length
                l2 = other.length
                return res / (l1 * l2)
            else:
                raise LookupError("Vectors not of equal length.")

    def dot(self,other):
        if isinstance(other,Vector):
            res = 0.0
            if self.row == other.row:
                for i in range(self.row):
                    res += self[i] * other[i]
                return res
            else:
                raise LookupError("Vectors not of equal length.")
#end Vector class


###############################
# MATRIX AND VECTOR FUNCTIONS #
###############################

#transpose matrix
def transpose(matx):
    row = matx.col
    col = matx.row
    rmat = Matrix(row, col, 0.0)
    for i in range(row):
        for j in range(col):
            rmat[i,j] = matx[j,i]
    return rmat

#dot product
def dot(v1, v2):
    size = len(v1)
    result = 0.0
    if size != len(v2):
        raise hou.Error("Uneven length of vectors.")
    else:
        for i in range(size):
             result = result + v1[i] * v2[i]
        return result

#trace matrix
def trace(matx):
    min_val = min([matx.row, matx.col])
    result = 0.0
    for i in range(min_val):
        result += matx[i,i]
    return result

#determinant of a 2 x 2 matrix
def det2D(matx):
    return matx[0,0]*matx[1,1] - matx[0,1] * matx[1,0]

#determinant of a 3 x 3 matrix
def det3D(matx):
    d1 = matx[1,1] * matx[2,2] - matx[1,2] * matx[2,1]
    d2 = matx[1,0] * matx[2,2] - matx[1,2] * matx[2,0]
    d3 = matx[1,0] * matx[2,1] - matx[1,1] * matx[2,0]
    return matx[0,0] * d1 - matx[0,1] * d2 + matx[0,2] * d3

#inverse matrix using Cramer's rule for size 2 and 3
#explicit method for size 4 and LU decomposition for
#matrices larger than 4
def invMatC(matx):
    size = matx.col
    if size == 2:
        rmat = Matrix(2, 2, 0.0)
        cdet = 1.0 / det2D(matx)
        rmat[0,0] =  cdet * matx[1,1]
        rmat[0,1] =  cdet * matx[0,1] * -1.0
        rmat[1,0] =  cdet * matx[1,0] * -1.0
        rmat[1,1] =  cdet * matx[0,0]
    elif size == 3:
        #problem with round of errors, incorporate something
        rmat = Matrix(3, 3, 0.0)
        cdet = 1.0 / det3D(matx)
        rmat[0,0] = cdet * ( matx[1,1] * matx[2,2] - matx[1,2] * matx[2,1] )
        rmat[0,1] = cdet * ( matx[1,0] * matx[2,2] - matx[1,2] * matx[2,0] ) * -1.0
        rmat[0,2] = cdet * ( matx[1,0] * matx[2,1] - matx[1,1] * matx[2,0] )
        rmat[1,0] = cdet * ( matx[0,1] * matx[2,2] - matx[0,2] * matx[2,1] ) * -1.0
        rmat[1,1] = cdet * ( matx[0,0] * matx[2,2] - matx[0,2] * matx[2,0] )
        rmat[1,2] = cdet * ( matx[0,0] * matx[2,1] - matx[0,1] * matx[2,0] ) * -1.0
        rmat[2,0] = cdet * ( matx[0,1] * matx[1,2] - matx[0,2] * matx[1,1] )
        rmat[2,1] = cdet * ( matx[0,0] * matx[1,2] - matx[0,2] * matx[1,0] ) * -1.0
        rmat[2,2] = cdet * ( matx[0,0] * matx[1,1] - matx[0,1] * matx[1,0] )
        rmat = transpose(rmat)  
    elif size == 4:
        cdet = 1.0 / explicitDetTet4(matx)
        rmat = inverseTet4(matx) * cdet
    else:
        #use the LU decomposition method here
        ident = Matrix(size, size, 0.0)
        ivec  = Vector(size, 1.0)
        ident.setDiagonal(ivec)
        (L, U, P) = LUdecomposition(matx, ident)
        LI = inverseL(L)
        UI = inverseU(U)
        rmat = UI * LI * P
    return rmat

#geometric centroid
def centroid(geom):
    cx = 0.0
    cy = 0.0
    cz = 0.0
    sz = len(geom.vertices())
    for i in range(size):
        cx =  cx + geom.vertices()[i].point().positions()[0]
        cy =  cy + geom.vertices()[i].point().positions()[1]
        cz =  cz + geom.vertices()[i].point().positions()[2]
    cx = cx * (1 / sz)
    cy = cy * (1 / sz)
    cz = cz * (1 / sz)
    centroid = [cx, cy, cz]
    return centroid

#return a slice of a matrix where rows and cols are the start
#row and column and nrow and ncol the number of rows and columns
#to copy
def sliceMatrix(matx, rows, cols, nrow, ncol):
    sizer = matx.size()[0]
    sizec = matx.size()[1]    
    res   = Matrix(nrow, ncol, 0.0)
    for i in range(0, nrow, 1):
        for j in range(0, ncol, 1):
            res[i,j] = matx[i + rows, j + cols]
    return res

#return a matrix where row and col are left out
def maskMatrix(matx,row,col):
    #size = matx.size()
    #if size[0] <=1 or size[1] <= 1:
    sizer = matx.size()[0]
    sizec = matx.size()[1]
    if sizer <= 1 or sizec <= 1:
        raise hou.Error("Can't create a matrix from a scalar or vector.")
    res   = Matrix(sizer -1, sizec -1, 0.0)
    for i in range(sizer):
        if i !=  row:
            for j in range(sizec):
                if j != col:
                    res[i,j] = matx[i,j]
    return res


##########################
# CONSTITUTIVE EQUATIONS #
##########################

#Strain matrix for 2D and 3D
def strainEMat(coordi,vol,DOF, NODES):
    if DOF == 2:
        B = Matrix(DOF + 1, NODES * DOF, 0.0)
        for i in range(NODES):
            B[0, i*DOF]   = coordi[i,0]
            B[1, i*DOF+1] = coordi[i,1]
            B[2, i*DOF]   = coordi[i,1]
            B[2, i*DOF+1] = coordi[i,0]
    if DOF == 3:
        B = Matrix(2 * DOF, NODES * DOF, 0.0)
        for i in range(NODES):
            B[0, i*DOF]   = coordi[i,0]
            B[1, i*DOF+1] = coordi[i,1]
            B[2, i*DOF+2] = coordi[i,2]
            B[3, i*DOF]   = coordi[i,1]
            B[3, i*DOF+1] = coordi[i,0]
            B[4, i*DOF+1] = coordi[i,2]
            B[4, i*DOF+2] = coordi[i,1]
            B[5, i*DOF]   = coordi[i,2]
            B[5, i*DOF+2] = coordi[i,0]
    B = B / (6.0 * vol)
    return B

#Constutive equation for elastic isotropic material in 3D
def constEqIso(young,poisson,DOF):
    mult = young / ((1 + poisson) * (1 - 2 * poisson))
    if DOF == 2:
        C = Matrix(DOF + 1, DOF + 1, 0.0)
        dia1 = 1.0 - poisson
        dia2 = 0.5 - poisson
        C[0,0] = dia1
        C[1,1] = dia1
        C[2,2] = dia2
        C[0,1] = poisson
        C[1,0] = C[0,1]
    else:
        C = Matrix(2 * DOF, 2 * DOF, 0.0)
        dia1 = 1.0 - poisson
        dia2 = 0.5 - poisson
        #diagonal elements
        C[0,0] = dia1
        C[1,1] = dia1
        C[2,2] = dia1
        C[3,3] = dia2
        C[4,4] = dia2
        C[5,5] = dia2
        #off diagonal elements
        C[0,1] = poisson
        C[0,2] = poisson
        C[1,2] = poisson
        C[1,0] = poisson
        C[2,0] = poisson
        C[2,1] = poisson
    C = C * mult
    return C



############################################
# General functions for 3D Finite Elements #
############################################

#Natural coordinate matrix
def naturalM(fun, DOF, NODES):
    res = Matrix(DOF,DOF*NODES,0.0)
    for i in range(NODES):
        for j in range(DOF):
            res[j,i*DOF+j] = fun[i]
    return res

#return the coordinates and weights of the gauss integration points
def gaussIntegrate(NGP):
    smp = Matrix(2, NGP)
    if NGP == 1:
        smp[0,0] = 0.0
        smp[1,0] = 2.0
    if NGP == 2:
        smp[0,0] = -0.5773502691896258
        smp[0,1] = -smp[0,0]
        smp[1,0] = 1.0
        smp[1,1] = smp[1,0]
    if NGP == 3:
        smp[0,0] = 0.7745966692414834
        smp[0,1] = 0.0
        smp[0,2] = -smp[0,0]
        smp[1,0] = 0.5555555555555556
        smp[1,1] = 0.8888888888888888
        smp[1,2] = smp[1,0]
    if NGP == 4:
        smp[0,0] = -0.8611363115940526
        smp[0,1] = -0.3399810435848563
        smp[0,2] = -smp[0,1]
        smp[0,3] = -smp[0,0]
        smp[1,0] = 0.34785484513745385
        smp[1,1] = 0.6521451548625462
        smp[1,2] = smp[1,1]
        smp[1,3] = smp[1,0]
    if NGP == 5:
        smp[0,0] = 0.906179845938664
        smp[0,1] = 0.538469310105683
        smp[0,2] = 0.0
        smp[0,3] = -smp[0,1]
        smp[0,4] = -smp[0,0]
    return smp


################################################
# Shape functions for 4 node tetrahedrons (3D) #
################################################

#function to get the proper node order. Returns the resorted coordination matrix.
def nodeOrderT(geom, order_vertices):
    #2012-06-29 changed order since v3 is the fourth node
    #2012-07-27 simplified things a bit
    if order_vertices == 0:
        v1 = geom.vertices()[0].point().position() - geom.vertices()[1].point().position()
        v2 = geom.vertices()[0].point().position() - geom.vertices()[2].point().position()
        v3 = geom.vertices()[0].point().position() - geom.vertices()[3].point().position()
        vd = v3.dot((v1.cross(v2)))
        if vd < 0:
            order = [0, 1, 2, 3]
            geom.setAttribValue("processed", 1)
        else:
            order = [0, 2, 1, 3]
            geom.setAttribValue("processed", -1)
    elif order_vertices == 1:
        order = [0, 1, 2, 3]
    elif order_vertices == -1:
        order = [0, 2, 1, 3]
    size = 4
    #is pointlist entry 0 equal to vertex 0?
    pointlist = geom.vertices()
    plist = [0] * size
#    xlist = [0.0] * size
#    ylist = [0.0] * size
#    zlist = [0.0] * size
#    i = 0
    rmat = Matrix(size, size, 1.0)
#    for i in xrange(size):
#        plist[i] = pointlist[order[i]].point().number()
#        xlist[i] = pointlist[order[i]].point().position()[0]
#        ylist[i] = pointlist[order[i]].point().position()[1]
#        zlist[i] = pointlist[order[i]].point().position()[2]
#    for j in xrange(size):
#        rmat[0, j] = xlist[j]
#        rmat[1, j] = ylist[j]
#        rmat[2, j] = zlist[j]
    for k in range(size):
        plist[k] = pointlist[order[k]].point().number()
        rmat[0, k] = pointlist[order[k]].point().position()[0]
        rmat[1, k] = pointlist[order[k]].point().position()[1]
        rmat[2, k] = pointlist[order[k]].point().position()[2]
    return (rmat, plist, order)

#explicit determinant of the 4x4 coordinate matrix
#multiply by (1/6) to get the volume of the tetrahedron
def explicitDetTet4(coord):
    x21 = coord[0,1] - coord[0,0]
    x32 = coord[0,2] - coord[0,1]
    x43 = coord[0,3] - coord[0,2]
    y12 = coord[1,0] - coord[1,1]
    y23 = coord[1,1] - coord[1,2]
    y34 = coord[1,2] - coord[1,3]
    z12 = coord[2,0] - coord[2,1]
    z23 = coord[2,1] - coord[2,2]
    z34 = coord[2,2] - coord[2,3]
    V = x21*(y23*z34 - y34*z23) + x32*(y34*z12 - y12*z34) + x43*(y12*z23 - y23*z12)
    return V

#explicit inversion of the 4x4 coordinate matrix
#the results needs to be divided by the determinant to get the final inversion
#but to be safe (in case the determinant is 0) do this after calling this function
#with a check of course
def inverseTet4(coord):
    x1 = coord[0,0]
    y1 = coord[1,0]
    z1 = coord[2,0]
    x2 = coord[0,1]
    y2 = coord[1,1]
    z2 = coord[2,1]
    x3 = coord[0,2]
    y3 = coord[1,2]
    z3 = coord[2,2]
    x4 = coord[0,3]
    y4 = coord[1,3]
    z4 = coord[2,3]
    JM = Matrix(4,4,0.0)
    #col 1 holds x
    JM[0,0] = (y4 - y2) * (z3 - z2) - (y3 - y2) * (z4 - z2)
    JM[1,0] = (y3 - y1) * (z4 - z3) - (y3 - y4) * (z1 - z3)
    JM[2,0] = (y2 - y4) * (z1 - z4) - (y1 - y4) * (z2 - z4)
    JM[3,0] = (y1 - y3) * (z2 - z1) - (y1 - y2) * (z3 - z1)
    #col 2 holds y
    JM[0,1] = (z4 - z2) * (x3 - x2) - (z3 - z2) * (x4 - x2)
    JM[1,1] = (z3 - z1) * (x4 - x3) - (z3 - z4) * (x1 - x3)
    JM[2,1] = (z2 - z4) * (x1 - x4) - (z1 - z4) * (x2 - x4)
    JM[3,1] = (z1 - z3) * (x2 - x1) - (z1 - z2) * (x3 - x1)
    #col 3 holds z
    JM[0,2] = (x4 - x2) * (y3 - y2) - (x3 - x2) * (y4 - y2)
    JM[1,2] = (x3 - x1) * (y4 - y3) - (x3 - x4) * (y1 - y3)
    JM[2,2] = (x2 - x4) * (y1 - y4) - (x1 - x4) * (y2 - y4)
    JM[3,2] = (x1 - x3) * (y2 - y1) - (x1 - x2) * (y3 - y1)
    #col 4 holds the sub tetrahedral volumes
    JM[0,3] = x2 * (y3 * z4 - y4 * z3) + x3 * (y4 * z2 - y2 * z4) + x4 * (y2 * z3 - y3 * z2)
    JM[1,3] = x1 * (y4 * z3 - y3 * z4) + x3 * (y1 * z4 - y4 * z1) + x4 * (y3 * z1 - y1 * z3)
    JM[2,3] = x1 * (y2 * z4 - y4 * z2) + x2 * (y4 * z1 - y1 * z4) + x4 * (y1 * z2 - y2 * z1)
    JM[3,3] = x1 * (y3 * z2 - y2 * z3) + x2 * (y1 * z3 - y3 * z1) + x3 * (y2 * z1 - y1 * z2)
    return JM


#interpolate a natural coordinate to world coordinate
def pointW(coord, natc):
    x = natc[0] * coord[0,0] + natc[1] * coord[0,1] + natc[2] * coord[0,2] + natc[3] * coord[0,3]
    y = natc[0] * coord[1,0] + natc[1] * coord[1,1] + natc[2] * coord[1,2] + natc[3] * coord[1,3]
    z = natc[0] * coord[2,0] + natc[1] * coord[2,1] + natc[2] * coord[2,2] + natc[3] * coord[2,3]
    res = [x, y, z, 1]
    return res

#interpolate a world coordinate to a natural coordinate
def pointN(coordi, pointWorld):
    x = pointWorld[0]
    y = pointWorld[1]
    z = pointWorld[2]
#    n1 = x * coordi[0,0] + x * coordi[0,1] + x * coordi[0,2] + x * coordi[0,3]
#    n2 = y * coordi[1,0] + y * coordi[1,1] + y * coordi[1,2] + y * coordi[1,3] 
#    n3 = z * coordi[2,0] + z * coordi[2,1] + z * coordi[2,2] + z * coordi[2,3]
    n1 = x * coordi[0,0] + y * coordi[0,1] + z * coordi[0,2]
    n2 = x * coordi[1,0] + y * coordi[1,1] + z * coordi[1,2] 
    n3 = x * coordi[2,0] + y * coordi[2,1] + z * coordi[2,2]
    n4 = 1.0 -n1 -n2 -n3
    res = [n1, n2, n3, n4]
    return res

#natural coordinate matrix N
def natCoordT(matx, vec1, num_nodes, dof):
    nat = Matrix(dof, num_nodes * dof, 0.0)
    x   = vec1[0]
    y   = vec1[1]
    z   = vec1[2]
    N1 = matx[0,0] * x + matx[0,1] * y + matx[0,2] * z + matx[0,3]
    N2 = matx[1,0] * x + matx[1,1] * y + matx[1,2] * z + matx[1,3]
    N3 = matx[2,0] * x + matx[2,1] * y + matx[2,2] * z + matx[2,3]
    N4 = 1 - N1 - N2 - N3
    nat[0, 0] = N1
    nat[1, 1] = N1
    nat[2, 2] = N1
    nat[0, 3] = N2
    nat[1, 4] = N2
    nat[2, 5] = N2
    nat[0, 6] = N3
    nat[1, 7] = N3
    nat[2, 8] = N3
    nat[0, 9] = N4
    nat[1,10] = N4
    nat[2,11] = N4
    return nat

#bogus stuff...is it useful?
def natDerT():
    der = Matrix(3, 4, 0.0)
    der[0,0] = 1.0
    der[1,1] = 1.0
    der[2,2] = 1.0
    der[0,3] = -1.0
    der[1,3] = -1.0
    der[2,3] = -1.0
    return der

#computes the unit interior normal of a face
#multiply by -1 to get the outward normal
def faceNormal(a, b, c):
    A = sqrt(a*a + b*b + c*c)
    an = a / A
    bn = b / A
    cn = c / A
    res = [an, bn, cn]
    return res

#Integration rules for a tetrahedron, Gaussian quadrature.
#The return values for 5 integration points differ in data structure.
def GaussIntT(NGP):
    if NGP == 1:
        smp = []
        for i in range(4):
            smp.append(0.25)
        wt = 1.0
    elif NGP == 4:
        smp = [0.0] * 4
        smp[1] = 0.58541020
        smp[2] = 0.13819660
        smp[3] = 0.13819660
        smp[4] = 0.58541020
        wt     = 0.25
    elif NGP == 5:
        smp = Matrix[2,4, 0.25]
        smp[1,0] = 0.5
        smp[1,1] = 1/6.0
        smp[1,2] = 1/6.0
        smp[1,3] = 1/6.0
        wt = [0.0] * 2
        wt[1] = -0.8
        wt[2] = 9/20.0
    return (smp, wt)

#integrate over a tetrahedron
def integrateTetra(formula):
    pass



###################################################
# SOLVER OF LINEAR SYSTEMS AND OTHER SOLVER STUFF #
###################################################

#First some functions to condition the different matrices

#row equilibration preconditioning
def rowEquilibration(matx, vec1):
    rows = matx.size()[0]
    if rows != len(vec1):
        raise hou.Error("Unequal rows in vector and matrix.")
    else:
        col = matx.size()[1]
        for i in range(rows):
            sum = 0.0
            for j in range(col):
                sum = sum + pow(matx[i,j], 2)
            root = sqrt(sum)
            for j in range(col):
                matx[i,j] = matx[i,j] / root
                vec1[i]   = vec1[i] / root
        #is this neccessary since stupid python makes hardcopies
        #of everything anyway?
        return (matx, vec1)

#scale rows, in combination with pivoting this results
#in more accurate solutions
def rowScaling(matx, vec1):
    maxval = 0.0
    for i in range(matx.row):
        for j in range(matx.col):
            if abs(matx[i,j]) > maxval:
                maxval = abs(matx[i,j])
        if maxval != 0:
            for k in range(matx.row):
                matx[i,k] = matx[i,k] / maxval
            vec1[i] = vec1[i] / maxval
    return(matx, vec1)

#residual pass for gaussian elimination methods
def residualPass(matx, sol, vec1, method, limit):
    vec2 = matx * sol
    vec3 = vec2 - vec1
    if method == 0:
        delta = Solver.GaussElim(matx, vec3)
    if method == 1:
        delta = Solver.CroutElim(matx, vec3)
    if method == 2:
        delta = Solver.GaussSeidel(matx, vec3, limit)
    #for i in range(size):
    sol = sol + delta
    return sol

#############################################
# Gauss elimination with partial pivoting   #
# stable!                                   #
#############################################
def GaussElim(matx, vec1):
    cmat = deepcopy(matx)
    cvec = deepcopy(vec1)
    status = "normal"
    #make this a variable?
    LIMIT = 0.5e-10
    i = 0
    n = cmat.size()[0]
    res = Vector(len(cvec), 0.0)
    while (i < n-1) and (status == "normal"):
        imax = i
        mmax = abs(cmat[i,i])
        #find the largest pivot element
        for k in range(i+1, n):
            if abs(cmat[k,i]) > mmax:
                imax = k
                mmax = abs(cmat[k,i])
        if abs(mmax) < LIMIT:
            status = "singular"
        if status == "normal":
            if i != imax:
                for j in range(i,n):
                    t = cmat[i,j]
                    cmat[i,j] = cmat[imax,j]
                    cmat[imax,j] = t
                t = cvec[i]
                cvec[i] = cvec[imax]
                cvec[imax] = t
            #sweep columns clean
            for l in range(i+1, n):
                if abs(cmat[l,i]) > LIMIT:
                    t = cmat[l,i]/cmat[i,i]
                    for m in range(i, n):
                        cmat[l,m] = cmat[l,m] - t * cmat[i,m]
                    cvec[l] = cvec[l] - t * cvec[i]
        i = i + 1
    #end of while statement, we arrived at last element
    #start of backsubstition
    if abs(cmat[n-1,n-1]) < LIMIT:
        status = "singular"
    if status == "normal":
        res[n-1] = cvec[n-1]/cmat[n-1,n-1]
        for i in range (n-2, -1, -1):
            res[i] = cvec[i]
            for j in range(i+1,n):
                res[i] = res[i] - cmat[i,j] * res[j]                
            res[i] = res[i]/cmat[i,i]
    #the solution
    return res

###################################################
# Crout Elimination, variant of LU Decomposition  #
# but wrapped in a single function.               #
###################################################
def CroutElim(matx, vec1):
    cmat = deepcopy(matx)
    cvec = deepcopy(vec1)
    n = cmat.size()[0]
    #in c++ check for singular (while statement)
    for l in xrange(1, n):
        #partial pivoting
        #find largest pivot element
        imax = l
        mmax = abs(cmat[l,l])
        for j in xrange(l+1):
            if abs(cmat[j,l]) > mmax:
                imax = j
                mmax = abs(cmat[j,l])
        if l != imax:
            for j in xrange(n):
                t = cmat[i,j]
                cmat[i,j] = cmat[imax, j]
                cmat[imax,j] = t
            t = cvec[i]
            cvec[i] = cvec[imax]
            cvec[imax] = t
        #upper
        for i in xrange(l, n):
            if l > 1:
                sums = 0.0
                for k in xrange(l-1):
                    sums += cmat[k,i] * cmat[l-1,k]
                cmat[l-1,i] = (cmat[l-1,i] - sums) / cmat[l-1,l-1]
            else: 
               cmat[l-1,i] = cmat[l-1,i] / cmat[l-1,l-1]
        #lower
        for i in xrange(l, n):
            sums = 0.0
            for k in xrange(l):
                sums += cmat[k,l] * cmat[i,k]
            cmat[i,l] = cmat[i,l] - sums 
    #calculate z elements
    res = Vector(n, 0.0)
    res[0] = cvec[0] / cmat[0,0]
    for i in range(1, n):
        sums = 0.0
        for j in range(i):
            sums += cmat[i,j] * res[j]
        res[i] = (cvec[i] - sums) / cmat[i,i]
    #back substitution
    for i in range(n-2, -1, -1):
        sums = 0.0
        for k in xrange(n-1, i, -1):
            sums += cmat[i,k] * res[k]
        res[i] = res[i] - sums
    return res

################################################################################
# LU Decomposition with partial pivoting. Input matx, matrix to be decomposed  #
# ident, identity matrix which acts as a permutation matrix in case rows are   #
# swapped. Unstable when the pivot elements equals 0.0                         #
################################################################################
def LUdecomposition(matx,ident):
    size = matx.size()[0]
    u = deepcopy(matx)
    l = Matrix(size,size,0.0)
    #start itterating
    for k in range(0,size):
        #pivoting seection
        imax = k
        mmax = abs(u[k,k])
        #find largest pivot element
        for j in range(k+1,size):
            if abs(u[j,k]) > mmax:
                imax = j
                mmax = abs(u[j,k])
        if k != imax:
            #we need to swap rows
            for j in range(size):
                ti = ident[k,j]
                tu = u[k,j]
                tl = l[k,j]
                ident[k,j] = ident[imax,j]
                ident[imax,j] = ti
                u[k,j] = u[imax,j]
                u[imax,j] = tu
                l[k,j] = l[imax,j]
                l[imax,j] = tl
        #end pivoting section
        #start processing upper part
        for i in range(k,size):
            sums = 0.0
            for j in range(0,k):
                sums = sums + l[k,j] * u[j,i]
            u[k,i] = u[k,i] - sums
            #set the diagonal of l to 1
            l[k,k] = 1.0
        for r in range(k+1,size):
            sums = 0.0
            for s in range(k):
                sums = sums + l[r,s] * u[s,k]
            l[r,k] = (u[r,k] - sums) / u[k,k]
        #end lower part
    #"beauty" pass
    for i in range(size):
        l[i,i]=1.0
        if i > 0:
            for j in range(i):
                u[i,j] = 0.0
    #and return the stuff
    return(l,u,ident)

# forward and backward substitution for solving
# linear equations
def LUsub(lower, upper, vec):
    size = len(vec)
    res  = Vector(size,0.0)
    # L * Y = vec
    #forward
    for j in range(0, size):
        sums = 0.0
        for i in range(0,j):
            sums = sums + lower[j,i] * res[i]
        res[j] = (vec[j] - sums)
    # U * X = Y
    #backward
    for i in range(size-1,-1,-1):
        sums = 0.0
        for j in range(size-1, i,-1):
            sums = sums + upper[i,j] * res[j]
        res[i] = (res[i] - sums) / upper[i,i]
    #return X
    return res

#Remember that when using the LU decomposition method
#for calculating the inverse matrix you have to use
#the permutation matrix if there has been row swapped!

#the inverse of the lower triangular matrix of LU
def inverseL(lower):
    l = deepcopy(lower)
    rows = l.row
    cols = l.col
    #multiply column * row with and add the pivot
    #move to the right
    for j in range(0, cols -1, 1):
        #move down
        for i in range(j+1, rows, 1):
            sums = 0.0
            #move diagonally
            for k in range(j+1, i,1):
                sums = sums + l[i,k] * l[k,j]
            l[i,j] = -1.0 * (l[i,j] + sums)
    return l

#the inverse of the upper triangular matrix of LU
def inverseU(upper):
    rows = upper.row -1
    cols = upper.col -1
    u = deepcopy(upper)
    #multiply row * column with ij as pivot
    #move up
    for i in range(rows, -1, -1):
        #move to the left
        for j in range(cols, i, -1):
            sums = 0.0
            #move down
            for k in range(i+1, j+1, 1):
                sums += u[i,k] * u[k,j]
            u[i,j] = -1.0 * sums / u[i,i]
        u[i,i] = 1.0 / u[i,i]
    return u

##################################################
# Cholesky functions for decomposing a symmetric #
# positive definite matrix. Issues when a pivot  #
# element equals 0. See also LUdecomposition     #
##################################################

#create a lower triangular matrix using Cholesky's method
#only works for positive definite matrices
def cholesky(matx):
    #lower triangle variant
    size = matx.size()[0]
    #res  = Matrix(size,size,0.0)
    res = deepcopy(matx)
    for k in range(size):
        res[k,k] = sqrt(res[k,k])
        for i in range(k+1, size):
            res[i,k] = res[i,k]/res[k,k]
        for j in range(k+1, size):
            for i in range(j, size):
                res[i,j] = res[i,j] - res[i,k] * res[j,k]
    for j in range(1,size):
        for i in range(j):
            res[i,j] = 0.0
    return res

#forward substitution for a lower triangular matrix
def cholfsub(matx,vec):
    size = len(vec)
    res = Vector(size, 0.0)
    #i is the row
    res[0] = vec[0] / matx[0,0]
    for i in range(1,size):
        sm = 0.0
        #j is the column
        for j in range(0,i):
            sm =  sm + matx[i,j] * res[j]
        sm = vec[i] - sm
        res[i] = sm / matx[i,i]
    return res

#backward substitution of an upper triangular matrix
#using a virtual transposed lower triangular matrix
def cholbsub(matx,vec):
    size = len(vec)
    n    = size -1
    res = Vector(size, 0.0)
    #i is the row
    res[n] = vec[n] / matx[n,n]
    for i in range(n-1,-1,-1):
        sums = 0.0
        #j is the column
        for j in range(n, i, -1):
            sums =  sums + matx[j,i] * res[j]
        sums = vec[i] - sums
        res[i] = sums / matx[i,i]
    return res

#forward and backward substitution of a lower
#triangular matrix decomposed by the Cholesky method.    
def cholsub(matx, vec):
    size = len(vec)
    n    = size -1
    res  = Vector(size,0.0)
    #i is the row
    res[0] = vec[0] / matx[0,0]
    for i in range(1,size):
        sums = 0.0
        #j is the column
        for j in range(0,i):
            sums =  sums + matx[i,j] * res[j]
        sums = vec[i] - sums
        res[i] = sums / matx[i,i]
    #start the backsubstitution
    res[n] = res[n] / matx[n,n]
    for i in range(n-1,-1,-1):
        sums = 0.0
        #j is the column
        for j in range(n, i, -1):
            sums =  sums + matx[j,i] * res[j]
        sums = res[i] - sums
        res[i] = sums / matx[i,i]
    return res

#below two itterative methods

###############################################################
# GaussSeidl iterative method (add preconditioning pivoting?) #
###############################################################
def GaussSeidel(matx, sol, res0, max_itter, tol):
    size = len(sol)
    res1 = Vector(size, 0.0)
    status = "normal"
    for k in range(size):
        if abs(matx[k,k]) < tol:
            print "Error"
            status = "singular"
    iter_num = 0
    if status == "normal":
        while (status == "normal") and (iter_num < max_itter):
            eps = 0.0
            for i in range(size):
                sum = 0.0
                for j in range(size):
                    sum = sum + matx[i,j] * res0[j]
                res1[i] = res0[i] + (sol[i] - sum) / matx[i,i]
                t = abs(res1[i] - res0[i])
                if t > eps:
                    eps = t
                res0[i] = res1[i]
            iter_num += 1
            if eps < tol:
                status = "done"
    return (res0, iter_num)


###############################################################
# Successive over relaxation                                  #
###############################################################
def SucOverRelax(matx, sol, res0, w, max_itter, tol):
    size = len(sol)
    res1 = Vector(size, 0.0)
    status = "normal"
    for k in range(size):
        if abs(matx[k,k]) < tol:
            print "Error"
            status = "singular"
    iter_num = 0
    if status == "normal":
        while (status == "normal") and (iter_num < max_itter):
            eps = 0.0
            for i in range(size):
                sum = 0.0
                for j in range(size):
                    sum = sum + matx[i,j] * res0[j]
                res1[i] = res0[i] + (sol[i] - sum) / matx[i,i]
                res1[i] = w * res1[i] + (1 - w) * res0[i]
                t = abs(res1[i] - res0[i])
                if t > eps:
                    eps = t
                res0[i] = res1[i]
            iter_num += 1
            if eps < tol:
                status = "done"
    return (res0, iter_num)

####################################################
# Conjugate Gradient method for element by element #
# solutions.                                       #
####################################################
def Conjugate(stiff, initial_sol, forces, max_loops, tol):
    #residual vector
    R = stiff * initial_sol - forces
    #direction vector
    D = -R
    #d = transpose of R0 * R0 aka dot product
    d  = R.lengthSquared
    #make sure dn is > tol initially
    dn = 2 * tol
    X = initial_sol
    counter = 0
    while counter < max_loops and dn > tol:
        U  = stiff * D
        l  = d / dot(D, U)
        X  = X + D * l
        R  = R + U * l
        dn = R.lengthSquared
        if sqrt(dn) > tol:
            a = dn / d
            D = -R + D * a
        counter += 1
        d = dn
    return (X, counter)


####################################################
# Functions for the  Jacobi eigenvalue and         #
# eigenvector algorithm                            #
####################################################

#construct the Givens rotation matrix
def Givens(c, s, pos):
    G = Matrix(3,3,0.0)
    if pos == 0:
        G[0,0] = 1.0
        G[1,1] = c
        G[2,2] = c
        G[1,2] = s
        G[2,1] = -s
    elif pos == 1:
        G[1,1] = 1.0
        G[0,0] = c
        G[2,2] = c
        G[0,2] = s
        G[2,0] = -s
    else:
        G[2,2] = 1.0
        G[0,0] = c
        G[1,1] = c
        G[0,1] = s
        G[1,0] = -s
    return G

#the jacobi eigenvalue algorithm for a 3x3 matrix
def JacobiEV(matx, max_iter):
    #identity matrix that in the end will
    #hold the different eigenvectors
    X = Matrix(3,3,0.0)
    X[0,0] = 1.0
    X[1,1] = 1.0
    X[2,2] = 1.0
    #tupples used for iterating over
    #the off diagonal matrix entries
    u = (0, 0, 1)
    v = (1, 2, 2)
    w = (2, 1, 0)
    #make copy of the matrix
    E = deepcopy(matx)
    for l in range(max_iter):
        for i, j, k in zip(u, v, w):
            if not E[i,j] == 0.0:
                beta = (E[j,j] - E[i,i]) / ( 2 * E[i,j] )
                sign = copysign(1.0, beta)
                t = (sign * beta) / (abs(beta) + sqrt(beta * beta +1))
                c = 1.0 / sqrt(t * t +1)
                s = c * t
                p = s / (1 + c)
                r1 = E[k,i]
                r2 = E[k,j]
                E[k,i] = r1 - s * (r2 + p * r1)
                E[i,k] = E[k,i]
                E[k,j] = r2 + s * (r1 - p * r2)
                E[j,k] = E[k,j]
                E[i,i] = E[i,i] - t * E[i,j]
                E[j,j] = E[j,j] + t * E[i,j]
                E[i,j] = 0.0
                E[j,i] = 0.0
                #code could be further optimized
                #by calculating the summation directly as we do
                #with the E matrix above
                GR = Givens(c, s, k)
                X = X * GR
    return (E, X)